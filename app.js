function books(array) {
  let ul = document.createElement("ul");
  array.forEach((element) => {
    try {
      if (!element.author) {
        throw new Error(`No author property`);
      }
      if (!element.name) {
        throw new Error(`No name property`);
      }
      if (!element.price) {
        throw new Error(`No price property`);
      }
      let li = document.createElement("li");
      li.textContent = `Автор: ${element.author} Ім'я: ${element.name} Ціна: ${element.price}`;
      ul.append(li);
    } catch (error) {
      console.log(error.message);
    }
  });

  document.querySelector(`#root`).append(ul);
}
books([
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
]);
